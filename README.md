Introduction
============

**ZUUPEE Admin** is a fully responsive administration template. Based on **[Bootstrap 4](https://getbootstrap.com)** framework.
Highly customizable and easy to use. Fits many screen resolutions from small mobile devices to large desktops.

**Download & Preview on [ZUUPEE ADMIN PANEL](https://zuupee.com)**

Looking for Premium Templates?
------------------------------
ZUUPEE.COM just opened a new premium templates page. Hand picked to insure the best quality and the most affordable
prices. Visit https://zuupee.com/premium for more information.

!["ZUUPEE Admin Panel"](https://zuupee.com/logo.png "ZUUPEE Admin Panel")

**ZUUPEE Admin Panel** has been carefully coded with clear comments in all of its JS, SCSS and HTML files.
SCSS has been used to increase code customizability.

Installation
------------
There are multiple ways to install ZUUPEE Admin Panel.

#### Download:

Download from Github or [visit ZUUPEE.COM](https://zuupee.com) and download the latest release.

#### Using The Command Line:

**NPM**
```bash
npm install zuupee-admin-panel
```

**Github**

- Clone to your machine
```
git clone https://github.com/zuupee/zuupee-admin-panel.git
```

Browser Support
---------------
- IE 10+
- Firefox (latest)
- Chrome (latest)
- Safari (latest)
- Opera (latest)

Contribution
------------
Contribution are always **welcome and recommended**! Here is how:

- Fork the repository ([here is the guide](https://help.github.com/articles/fork-a-repo/)).
- Clone to your machine ```git clone https://github.com/YOUR_USERNAME/ZUUPEE Admin.git```
- Create a new branch
- Make your changes
- Create a pull request

#### Contribution Requirements:

- When you contribute, you agree to give a non-exclusive license to ZUUPEE.COM to use that contribution in any context as we (ZUUPEE.COM) see appropriate.
- If you use content provided by another party, it must be appropriately licensed using an [open source](http://opensource.org/licenses) license.
- Contributions are only accepted through Github pull requests.
- Finally, contributed code must work in all supported browsers (see above for browser support).

License
-------
ZUUPEE Admin Panel is an open source project by [ZUUPEE.COM](https://zuupee.com) that is licensed under [MIT](http://opensource.org/licenses/MIT). ZUUPEE.COM
reserves the right to change the license of future releases.

Legacy Releases
---------------
- [ZUUPEE Admin Panel 1](https://github.com/ZUUPEE/ADMIN/releases/tag/1.0.0)

Change log
----------
Visit the [releases](https://github.com/ZUUPEE/ADMIN/releases) page to view the changelog

Image Credits
-------------
[Pixeden](http://www.pixeden.com/psd-web-elements/flat-responsive-showcase-psd)

[Graphicsfuel](http://www.graphicsfuel.com/2013/02/13-high-resolution-blur-backgrounds/)

[Pickaface](http://pickaface.net/)

[Unsplash](https://unsplash.com/)

[Uifaces](http://uifaces.com/)
