import babel from 'rollup-plugin-babel'

const pkg  = require('./package')
const year = new Date().getFullYear()

const globals = {
  jquery: 'jQuery'
}

export default {
  input  : 'src/js/zuupee.js',
  output : {
    banner: `/*!
 * ZUUPEE Admin v${pkg.version} (${pkg.homepage})
 * Copyright 2018-${year} ${pkg.author}
 * Licensed under MIT (https://bitbucket.org/zuupee/zuupee-admin/src/master/LICENSE)
 */`,
    file  : 'dist/js/zuupee.js',
    format: 'umd',
    globals,
    name  : 'zuupee'
  },
  plugins: [
    babel({
      exclude: 'node_modules/**'
    })
  ]
}
